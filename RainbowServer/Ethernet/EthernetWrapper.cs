﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace RainbowServer.Ethernet
{
    class EthernetWrapper
    {
        private Thread _listenerThread;
        public String IpAddress { get; private set; }
        public int SocketPort { get; private set; }
        public TcpClient client { get; private set; }

        public EthernetWrapper(String ipAddress, int socketPort)
        {
            this.IpAddress = ipAddress;
            this.SocketPort = socketPort;
            try
            {
                client = new TcpClient(this.IpAddress, SocketPort);
            }
            catch
            {
                Console.WriteLine("Couldn't open TCP connection with host: {0} at port: {1}", IpAddress, SocketPort);
            }
        }

        public void sendString(string message, Action<string> completion)
        {
            new Thread(() =>
            {
                try
                {
                    var stream = client.GetStream();
                    var data = Encoding.ASCII.GetBytes(message + "\n");

                    stream.Write(data, 0, data.Length);

                    if (completion != null)
                    {
                        completion(message);
                    }
                }
                catch
                {
                    Console.WriteLine("Couldn't send data to host: {0} at port: {1}", IpAddress, SocketPort);
                }
            }).Start();
        }

        public void runTCPListener(Action<String> receivedMessageHandler)
        {
            if (_listenerThread != null)
            {
                _listenerThread.Interrupt();
            }
            _listenerThread = new Thread(() =>
            {
                try
                {
                    Console.WriteLine("TCPListener started!");

                    NetworkStream inStream = client.GetStream();

                    Byte[] bytes = new Byte[256];
                    String message = null;

                    int i;

                    while ((i = inStream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        message = Encoding.ASCII.GetString(bytes, 0, i);
                        if (receivedMessageHandler != null)
                        {
                            receivedMessageHandler(message);
                        }
                    }

                    inStream.Close();
                    client.Close();
                }
                catch (SocketException e)
                {
                    Console.WriteLine("SocketException: {0}", e);
                }
                catch (IOException ioe)
                {
                    client.Close();
                }
                catch (ObjectDisposedException ODE)
                {

                }
                finally
                {
                    client.Close();
                }
            });
            _listenerThread.Start();
        }

        public void closeListener()
        {
            if (_listenerThread != null)
            {
                _listenerThread.Interrupt();
            }
        }
    }
}
