﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RainbowServer.Ethernet
{
    public partial class Debugger : Form
    {
        private EthernetWrapper ethernetWrapper;
        public Debugger()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            ethernetWrapper.sendString("q", null);
            ethernetWrapper.closeListener();
            ethernetWrapper.client.Close();

            base.OnClosing(e);
        }

        private void button1_Click(object sender, EventArgs e) //send
        {
            ethernetWrapper.sendString(inputTxtBx.Text, ((message) =>
            {
                MessageBox.Show("Wysłano: " + message);
            }));
        }

        private void button1_Click_1(object sender, EventArgs e) //connect
        {
            ethernetWrapper = new EthernetWrapper(ipBox.Text, int.Parse(socketBox.Text));
            ethernetWrapper.runTCPListener((message) =>
            {
                Console.Out.WriteLine(message);
                try
                {
                    var hash = JSONDeserializer.deserializeMessage(message);
                }
                catch
                {
                    Console.Out.WriteLine("Nie można zdeserializować");
                }
                this.Invoke((MethodInvoker)delegate
                {
                    outputTxtBx.AppendText(new DateTime().ToShortTimeString() + " " + message + "\r\n");
                });
            });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ethernetWrapper.sendString("q", ((message) =>
            {
                Console.Out.WriteLine("Zakończono połączenie");
                ethernetWrapper.closeListener();
                ethernetWrapper.client.Close();
            }));
        }
    }
}
