﻿using RainbowServer.Ethernet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using RainbowServer.Generator;
using System.Security.Cryptography;
using System.Threading;
using System.Collections.ObjectModel;

namespace RainbowServer
{
    public partial class MainForm : Form
    {
        private OpenFileDialog openfileDialog = new OpenFileDialog();
        private AddHashesForm form2 = new AddHashesForm();
        private RainbowGeneratorForm form3 = new RainbowGeneratorForm();
        private PasswordGenerator passGen = new PasswordGenerator();
        private PasswordGeneratorFPGA passFPGA = new PasswordGeneratorFPGA();

        Collection<Thread> threads = new Collection<Thread>();

        String reductionOfCrackingHash = "";
        String currentCrackingHash = "";

        public MainForm()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        private void searchRainbowTablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openfileDialog.ShowDialog() == DialogResult.OK)
            {
                //odczytaj dane z pliku u wyswietl w dialogu
                string name = openfileDialog.SafeFileName;
                string path = openfileDialog.FileName;
                //Console.WriteLine("name: " + name);
                //Console.WriteLine("path: " + path);
                string ext = Path.GetExtension(path);
                richTextBox3.AppendText("[Info] Rainbow Table file was loaded successful!");
                Console.WriteLine("Plik z teczowa tablica zaladowany");
                Console.WriteLine("Obsluzyc odczyt danych z pliku i ogolna procedure crackowania hasha");
                //....
                
                
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void tableGenerated()
        {
            MessageBox.Show("Rainbow table generated successful!");
        }


        //generate rainbow table in hardware
        private void generateRainbowTableInHardwareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form3.ShowDialog();
            /*var shaProv = new SHA1CryptoServiceProvider();

            bool isNumeric = false;
            bool isAlpha = false;
            bool isLower = false;

            Console.WriteLine("SHA: " + form3.alg);
            foreach (string x in form3.charsets)
            {
                if (x.Equals("numeric"))
                    isNumeric = true;
                if (x.Equals("alpha"))
                    isAlpha = true;
                if (x.Equals("lower"))
                    isLower = true;

                Console.WriteLine("* " + x);
            }
            Console.WriteLine("Metoda redukcji: " + form3.reduction);
            Console.WriteLine("Min L hasla: " + form3.minL);
            Console.WriteLine("Max L hasla: " + form3.maxL);
            Console.WriteLine("Liczba lancuchow: " + form3.chainNo);
            Console.WriteLine("Dlugosc lancuchow: " + form3.chainL);


            //Generate passowrds
            if (isNumeric && isAlpha && isLower)
            {
                for (int i = 0; i < form3.chainNo; i++)
                {
                    var newThread = new Thread(() => {
                        String generatedPass = passFPGA.numericAndLowerAndAlpha("", form3.maxL, form3.chainL, form3.chainNo, form3.reduction);
                        Console.WriteLine("generated pass: " + generatedPass);
                        // Uruchomić połączenie z FPGA per instancja klasy + obsłużyć odpowiedź

                    });
                    threads.Add(newThread);
                    newThread.Start();
                }
               
            }
            
            else if (isNumeric)
            {
                var newThread = new Thread(() => passFPGA.numeric("", form3.maxL, form3.chainL, form3.chainNo));
                threads.Add(newThread);
                newThread.Start();
            }
            */
        }


        // *************************************************************************************
        // generate rainbow table in soft
        private void generateRainbowTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form3.ShowDialog();
            var shaProv = new SHA1CryptoServiceProvider();

            bool isNumeric = false;
            bool isAlpha = false;
            bool isLower = false;

            Console.WriteLine("SHA: " + form3.alg);
            foreach (string x in form3.charsets)
            {
                if (x.Equals("numeric"))
                    isNumeric = true;
                if (x.Equals("alpha"))
                    isAlpha = true;
                if (x.Equals("lower"))
                    isLower = true;

                Console.WriteLine("* " + x);
            }
            Console.WriteLine("Metoda redukcji: " + form3.reduction);
            Console.WriteLine("Min L hasla: " + form3.minL);
            Console.WriteLine("Max L hasla: " + form3.maxL);
            Console.WriteLine("Liczba lancuchow: " + form3.chainNo);
            Console.WriteLine("Dlugosc lancuchow: " + form3.chainL);

                        
            //Generate passowrds
            if (isNumeric && isAlpha && isLower)
            {
                var newThread = new Thread(() => passGen.numericAndLowerAndAlpha(this, "", form3.maxL, shaProv, form3.chainL, form3.chainNo));
                threads.Add(newThread);
                newThread.Start();


            }
                
            else if (isNumeric)
            {
                var newThread = new Thread(() => passGen.numeric("", form3.maxL, shaProv, form3.chainL, form3.chainNo));
                threads.Add(newThread);
                newThread.Start();
            }
            


        }

        private void addHashesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form2.ShowDialog();
            try
            {
                listBox1.Items.Clear();
                listBox2.Items.Clear();
                string[] hashes = form2.hashes;
                for (int i = 0; i <= hashes.Length - 1; i++)
                {
                    System.Console.WriteLine(hashes[i]);
                    listBox1.Items.Add(hashes[i]);
                }

            }
            catch (System.NullReferenceException)
            {
                Console.WriteLine("Blad odczytu danych");
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openfileDialog.ShowDialog() == DialogResult.OK)
            {
                listBox1.Items.Clear();
                //odczytaj dane z pliku u wyswietl w dialogu
                string name = openfileDialog.SafeFileName;
                string path = openfileDialog.FileName;
                //Console.WriteLine("name: " + name);
                //Console.WriteLine("path: " + path);
                string ext = Path.GetExtension(path);
                if (ext == ".txt")
                {
                    Console.WriteLine("Odczyt danych z pliku");
                    List<string> hashes = new List<string>();
                    hashes = readHashesFromFile(path);
                    listBox1.Items.Clear();
                    foreach (string hash in hashes)
                    {
                        if (hash.Length == 40)
                        {
                            listBox1.Items.Add(hash);
                        }
                        else
                        {
                            richTextBox3.AppendText("[Warning] Invalid length for a SHA-1 hash: " + hash);
                        }

                    }
                }
                else
                {
                    richTextBox3.AppendText("[Warning] Only .txt extension is accepted!");
                }


            }
        }

        private List<string> readHashesFromFile(string path)
        {
            List<string> hashes = new List<string>();
            int counter = 0;
            string hash;
            try
            {
                if (System.IO.File.Exists(path))
                {
                    System.Console.WriteLine("Plik istnieje");
                }
                else
                {
                    System.Console.WriteLine("Plik nie istnieje");
                }
                System.IO.StreamReader file = new System.IO.StreamReader(path);
                while ((hash = file.ReadLine()) != null)
                {
                    if (!hash.Equals(""))
                    {
                        System.Console.WriteLine(hash);
                        hashes.Add(hash);
                        counter++;
                    }

                }
                file.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            return hashes;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        void success(string password, string reduction)
        {
            //string pass = crackReduction(password, reduction);
            //Console.WriteLine("[Correct] Password to hash: " + currentCrackingHash + " is : " + pass);
            listBox2.Invoke(new Action(() =>
            {
                listBox2.Items.Add(crackReduction(password, reduction));
            }));
        }
        int i = 0;
        void failure(String oldReduction)
        {
            if (i < 10000)
            {
                i++;
                //What to do when end of chain is diffrent than input hash
                // 1. Reduce input hash
                // 2. Hash output of reduce
                // 3. Check if hash of reduce output is in rainbow table
                // 3a. yes - do points 1-4 (like when input hash equals to end of chain in rainbow table
                // 3b. no - repeat points 1-3, if after chainL 

                //Console.WriteLine("[Reduction " + (i++) +"] Trying one more time");
                Generator.SHA1 sha1 = new Generator.SHA1();
                string hash = sha1.bytesToHex(sha1.generateSHA1Hash(oldReduction, new SHA1CryptoServiceProvider()));

                DB.RainbowDBManager.findPassForHash(reduce1(hash), success, failure);
            }
            else
                MessageBox.Show("Cracking failed!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //crackowanie hasel
            //1. Pobierz hashe z dialogu
            //2. Wez kolejne hasze (petla)
            //3. policz redukcje dla hasha
            //4. sprawdz redukcje w bazie
            //5. jesli jest zwroc success; jesli nie:
            //      - policz hash z redukcji
            //      - wez redukcje z hasha potworz od kroku 4.
           
            // Rozwiązany problem z pętlami na ActionHandlerach.
            // Popatrzeć wyżej i niżej od komentarza ;)
            foreach (string hash in listBox1.Items)
            {
                reductionOfCrackingHash = reduce1(hash);
                currentCrackingHash = hash;
                if (reductionOfCrackingHash == "")
                    break;

                DB.RainbowDBManager.findPassForHash(reductionOfCrackingHash, success, failure);
            }
        }

        private string reduce1(string hash)
        {
            Generator.SHA1 sha1 = new Generator.SHA1();
            var shaProv = new SHA1CryptoServiceProvider();
            string reduction = sha1.countReductionString(hash, 5); //na szty dlugosc redukcji (na razie)
            return reduction;
        }

        private string crackReduction(string passIN, string reductionFromCorrectRainbowRow)
        {
            string pass = passIN;
            //char[] reductionChars = new char[5];                //na sztywno dlugosc hasel
            Generator.SHA1 sha1 = new Generator.SHA1();
            var shaProv = new SHA1CryptoServiceProvider();
            string nextReduction = "";
            for (int i = 0; i < 10000; i++)                     //na sztywno liczba iteracji petli;
            {
                byte[] hashB = sha1.generateSHA1Hash(pass, shaProv);
                
                nextReduction = reduce1(sha1.bytesToHex(hashB));
                if (i < 10)
                Console.WriteLine("Current reduction: " + nextReduction + ";   " + reductionOfCrackingHash);
                //pass generated equal hash so return this pass
                if (reductionOfCrackingHash == nextReduction)
                    break;
                else //else to reduce hash and find good pass
                {
                    pass = nextReduction;
                    
                }

                //Protection: when pass has not been found after trying 'chainL' times
                if (i == (10000 - 1) && reductionFromCorrectRainbowRow != nextReduction) //na sztywno: przerwanie liczenia
                    pass = "[Error] Password has not been found!";
            }
            return pass;
        }

        private string reduce(string hash)
        {
            
            char[] reductionChars = new char[form3.maxL];
            Generator.SHA1 sha1 = new Generator.SHA1();
            var shaProv = new SHA1CryptoServiceProvider();
            string pass = "";

            byte[] hash1 = Enumerable.Range(0, hash.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(hash.Substring(x, 2), 16))
                     .ToArray();

            for (int k = 0; k < form3.maxL; k++)
            {
                lock (reductionChars)
                {
                    //Console.WriteLine(sha1.byteToHex(hashB[k]));
                    reductionChars[k] = sha1.getReductionCharFromByte(hash1[k]);
                }
            }
            lock (reductionChars)
            {
                pass = new string(reductionChars);
            }
            byte[] hashB = sha1.generateSHA1Hash(pass, shaProv);

            return sha1.bytesToHex(hashB);
        }


        private void ethernetDebuggerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Ethernet.Debugger().Show();
        }
    }
}
