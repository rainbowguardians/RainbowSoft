﻿namespace RainbowServer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addHashesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rainbowTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchRainbowTablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateRainbowTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateRainbowTableInHardwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.ethernetDebuggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.rainbowTableToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(586, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addHashesToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // addHashesToolStripMenuItem
            // 
            this.addHashesToolStripMenuItem.Name = "addHashesToolStripMenuItem";
            this.addHashesToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.addHashesToolStripMenuItem.Text = "Add hashes";
            this.addHashesToolStripMenuItem.Click += new System.EventHandler(this.addHashesToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.loadToolStripMenuItem.Text = "Load hashes from file";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // rainbowTableToolStripMenuItem
            // 
            this.rainbowTableToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchRainbowTablesToolStripMenuItem,
            this.generateRainbowTableToolStripMenuItem,
            this.generateRainbowTableInHardwareToolStripMenuItem,
            this.ethernetDebuggerToolStripMenuItem});
            this.rainbowTableToolStripMenuItem.Name = "rainbowTableToolStripMenuItem";
            this.rainbowTableToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.rainbowTableToolStripMenuItem.Text = "Rainbow Table";
            // 
            // searchRainbowTablesToolStripMenuItem
            // 
            this.searchRainbowTablesToolStripMenuItem.Name = "searchRainbowTablesToolStripMenuItem";
            this.searchRainbowTablesToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.searchRainbowTablesToolStripMenuItem.Text = "Search rainbow table";
            this.searchRainbowTablesToolStripMenuItem.Click += new System.EventHandler(this.searchRainbowTablesToolStripMenuItem_Click);
            // 
            // generateRainbowTableToolStripMenuItem
            // 
            this.generateRainbowTableToolStripMenuItem.Name = "generateRainbowTableToolStripMenuItem";
            this.generateRainbowTableToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.generateRainbowTableToolStripMenuItem.Text = "Generate rainbow table in software";
            this.generateRainbowTableToolStripMenuItem.Click += new System.EventHandler(this.generateRainbowTableToolStripMenuItem_Click);
            // 
            // generateRainbowTableInHardwareToolStripMenuItem
            // 
            this.generateRainbowTableInHardwareToolStripMenuItem.Name = "generateRainbowTableInHardwareToolStripMenuItem";
            this.generateRainbowTableInHardwareToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.generateRainbowTableInHardwareToolStripMenuItem.Text = "Generate rainbow table in hardware";
            this.generateRainbowTableInHardwareToolStripMenuItem.Click += new System.EventHandler(this.generateRainbowTableInHardwareToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Hashes";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(393, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Plaintext";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(311, 109);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 37);
            this.button1.TabIndex = 5;
            this.button1.Text = "Crack hashes";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(30, 249);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(532, 280);
            this.richTextBox3.TabIndex = 6;
            this.richTextBox3.Text = "";
            this.richTextBox3.TextChanged += new System.EventHandler(this.richTextBox3_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Messages";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(30, 55);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(275, 147);
            this.listBox1.TabIndex = 9;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(396, 54);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(165, 147);
            this.listBox2.TabIndex = 10;
            // 
            // ethernetDebuggerToolStripMenuItem
            // 
            this.ethernetDebuggerToolStripMenuItem.Name = "ethernetDebuggerToolStripMenuItem";
            this.ethernetDebuggerToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.ethernetDebuggerToolStripMenuItem.Text = "Ethernet debugger";
            this.ethernetDebuggerToolStripMenuItem.Click += new System.EventHandler(this.ethernetDebuggerToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 546);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.richTextBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MainForm";
            this.Text = "Rainbow Cracker";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addHashesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rainbowTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchRainbowTablesToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ToolStripMenuItem generateRainbowTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateRainbowTableInHardwareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ethernetDebuggerToolStripMenuItem;
    }
}

