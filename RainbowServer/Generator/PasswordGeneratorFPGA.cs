﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RainbowServer.Generator
{
    /*
     * Generate init passwords and sent them to FPGA
     * */
    class PasswordGeneratorFPGA
    {
        int firstNumeric = 48; //0
        int lastNumeric = 57; //9
        int firstAlpha = 65; //A
        int lastAlpha = 90; //Z
        int firstLower = 97; //a
        int lastLower = 102; //z - 122; f - 102
        int chainsCounter = 0;
        int charsetCounter = 0;

        List<string> generatedPasses = new List<string>();


        public List<string> getGeneratedPasses()
        {
            return generatedPasses;
        }
        // Zmiany dotyczą sposobu generacji hasła, 
        // ze względu na zorientowaną połączeniowo komunikację z FPGA 
        // wymagana jest zwrotka do centralnego zarządcy.
        public void numericAndLowerAndAlpha(string prefix, int passL, int chainL, int chainNo, string reduction)
        {
            //Console.WriteLine("FPGA generating");
            if (reduction == "method 1" || reduction == "method 2")
            {
                for (int c = firstNumeric; c <= lastLower; c++)
                {
                    if ((c > 57 && c < 97))// || (c > 90 && c < 97))
                    {
                        continue;
                    }
                    else
                    {
                        if (prefix.Length == passL)
                        {
                            generatedPasses.Add(prefix);
                            chainsCounter++;
                            return;
                        }
                        if (chainsCounter < chainNo)
                            numericAndLowerAndAlpha(prefix + (char)c, passL, chainL, chainNo, reduction);
                        if (chainsCounter == chainNo)
                            chainsCounter += 1;

                    }
                }
            }

        }

        /*
        public void numeric(string prefix, int passL, int chainL, int chainNo)
        {
            if (prefix.Length == passL)
                chainsCounter++;

            //generate numeric
            for (int c = firstNumeric; c <= lastNumeric; c++)
            {
                if (prefix.Length == passL)
                {
                    //send to FPGA
                }

                if (prefix.Length == passL)
                    //Kod redukcji dla prefix'u jako plaintextu o dlugosci stringsLength 
                    //(jesli tutaj bedzie kod redukcji to redukcja bedzie wykonywana tylko dla 
                    //hasel o dlugosci stringslength bez hasel krotszych
                    //string reduction
                    return;
                if (chainsCounter < chainNo)
                {

                    numeric(prefix + (char)c, passL, chainL, chainNo);
                }

            }
        }
        */

    }
}
