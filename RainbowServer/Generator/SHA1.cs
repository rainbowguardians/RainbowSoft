﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RainbowServer.Generator
{
    class SHA1
    {

        public string getReductionString(string plaintext, SHA1CryptoServiceProvider shaProv, int maxPassL)
        {
            //string reduction = new string('x');
            char[] reductionChars = new char[maxPassL];
            byte[] hashBytes = generateSHA1Hash(plaintext, shaProv);
            for (int i = 0; i < maxPassL; i++)
            {
                reductionChars[i] = getReductionCharFromByte(hashBytes[i]);
            }

            string reduction = new string(reductionChars);
            Console.WriteLine(reduction);

            return reduction;
        }

        public byte[] generateSHA1Hash(string plaintext, SHA1CryptoServiceProvider shaProv)
        {
            byte[] hashvalue = shaProv.ComputeHash(Encoding.Default.GetBytes(plaintext));
            StringBuilder str = new StringBuilder();
            return hashvalue;

        }

        public char getReductionCharFromByte(byte reductionByte)
        {
            int number = convertByteToInt(reductionByte);
            return countReductionChar(number);
        }

        public int convertByteToInt(byte reductionByte)
        {
            int reductionInt = Convert.ToInt32(reductionByte);
            int number = 0;

            if (reductionInt >= 0 && reductionInt <= 63)
                number = reductionInt;
            else if (reductionInt >= 64 && reductionInt <= 127)
                number = reductionInt - 64;
            else if (reductionInt >= 128 && reductionInt <= 191)
                number = reductionInt - 128;
            else
                number = reductionInt - 192;

            return number;
        }

        public string bytesToHex(byte[] bytes)
        {
            StringBuilder hex = new StringBuilder();
            foreach (byte b in bytes)
                hex.AppendFormat("{0:x2}", b);

            return hex.ToString().ToLower();
        }

        public string byteToHex(byte b)
        {
            StringBuilder hex = new StringBuilder();
            hex.AppendFormat("{0:x2}", b);

            return hex.ToString().ToLower();
        }

        public int hexToInt(string hex)
        {
            int number = 0;
            number = Convert.ToInt32(hex, 16);

            return number;
        }

        public byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public string countReductionString(string hash, int passL)
        {
            string reduction = "";
            for (int i = 0; i < passL; i++)
                reduction += hash[i].ToString();

            return reduction;
        }

        public char countReductionChar(int reductionInt)
        {
            char result = '0';

            switch (reductionInt)
            {

                case 0:
                    result = 'a';
                    break;

                case 1:
                    result = 'b';
                    break;

                case 2:
                    result = 'c';
                    break;

                case 3:
                    result = 'd';
                    break;

                case 4:
                    result = 'e';
                    break;

                case 5:
                    result = 'f';
                    break;

                case 6:
                    result = 'g';
                    break;

                case 7:
                    result = 'h';
                    break;

                case 8:
                    result = 'i';
                    break;

                case 9:
                    result = 'j';
                    break;

                case 10:
                    result = 'k';
                    break;

                case 11:
                    result = 'l';
                    break;

                case 12:
                    result = 'm';
                    break;

                case 13:
                    result = 'n';
                    break;

                case 14:
                    result = 'o';
                    break;

                case 15:
                    result = 'p';
                    break;

                case 16:
                    result = 'q';
                    break;

                case 17:
                    result = 'r';
                    break;

                case 18:
                    result = 's';
                    break;

                case 19:
                    result = 't';
                    break;

                case 20:
                    result = 'u';
                    break;

                case 21:
                    result = 'v';
                    break;

                case 22:
                    result = 'w';
                    break;

                case 23:
                    result = 'x';
                    break;

                case 24:
                    result = 'y';
                    break;

                case 25:
                    result = 'z';
                    break;

                case 26:
                    result = 'A';
                    break;

                case 27:
                    result = 'B';
                    break;

                case 28:
                    result = 'C';
                    break;

                case 29:
                    result = 'D';
                    break;

                case 30:
                    result = 'E';
                    break;

                case 31:
                    result = 'F';
                    break;

                case 32:
                    result = 'G';
                    break;

                case 33:
                    result = 'H';
                    break;

                case 34:
                    result = 'I';
                    break;

                case 35:
                    result = 'J';
                    break;

                case 36:
                    result = 'K';
                    break;

                case 37:
                    result = 'L';
                    break;

                case 38:
                    result = 'M';
                    break;

                case 39:
                    result = 'N';
                    break;

                case 40:
                    result = 'O';
                    break;

                case 41:
                    result = 'P';
                    break;

                case 42:
                    result = 'Q';
                    break;

                case 43:
                    result = 'R';
                    break;

                case 44:
                    result = 'S';
                    break;

                case 45:
                    result = 'T';
                    break;

                case 46:
                    result = 'U';
                    break;

                case 47:
                    result = 'V';
                    break;

                case 48:
                    result = 'W';
                    break;

                case 49:
                    result = 'X';
                    break;

                case 50:
                    result = 'Y';
                    break;

                case 51:
                    result = 'Z';
                    break;

                case 52:
                    result = '0';
                    break;

                case 53:
                    result = '1';
                    break;

                case 54:
                    result = '2';
                    break;

                case 55:
                    result = '3';
                    break;

                case 56:
                    result = '4';
                    break;

                case 57:
                    result = '5';
                    break;

                case 58:
                    result = '6';
                    break;

                case 59:
                    result = '7';
                    break;

                case 60:
                    result = '8';
                    break;

                case 61:
                    result = '9';
                    break;

                case 62:
                    result = '.';
                    break;

                case 63:
                    result = '_';
                    break;
            }

            return result;
        }     

    }

}