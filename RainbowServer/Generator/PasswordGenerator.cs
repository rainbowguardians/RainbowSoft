﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RainbowServer.Generator
{
    /*
     * Generate init passwords and count reduction function in soft
     * 
     * */
    class PasswordGenerator
    {
        int firstNumeric = 48; //0
        int lastNumeric = 57; //9
        int firstAlpha = 65; //A
        int lastAlpha = 90; //Z
        int firstLower = 97; //a
        int lastLower = 122; //z
        int chainsCounter = 0;
        private SHA1 sha1 = new SHA1();

        volatile SortedList<String, String> reductionList = new SortedList<String, String>();

        public void generateRainbowRow(string prefix, int passL, SHA1CryptoServiceProvider shaProv, int chainL)
        {
            string pass = prefix;
            char[] reductionChars = new char[passL];
            string lastHashDB = "";
            //Console.WriteLine("\n*************************************\nPass: " + pass);

            for (int i = 0; i < chainL; i++)
            {
                byte[] hashB = sha1.generateSHA1Hash(pass, shaProv);
                for (int k = 0; k < passL; k++)
                {
                    lock (reductionChars)
                    {
                        //Console.WriteLine(sha1.byteToHex(hashB[k]));
                        reductionChars[k] = sha1.getReductionCharFromByte(hashB[k]);
                    }
                }
                lock (reductionChars)
                {
                    if (reductionList.ContainsKey(pass))
                    {
                        //przeciwdzialanie powtorzeniu hasla w przypadku redukcji
                    }
                    pass = new string(reductionChars);
                    
                           
                }
                //Console.WriteLine("Hash: " + sha1.bytesToHex(hashB) + "\nRedukcja: " + pass);
                if (i == chainL - 1) //get last hash to DB
                {
                    lastHashDB = sha1.bytesToHex(hashB);
                    //Console.WriteLine("RainbowRow (" + prefix + ", " + lastHashDB + ")");
                    //save to database: Insert rainbowRow(prefix, lastHashDB)
                    DB.RainbowDBManager.writeToDB(prefix, lastHashDB);
                    Console.WriteLine("Zapisano do bazy: (" + prefix + ", " + lastHashDB + ")");
                    reductionList.Add(prefix, lastHashDB);
                }
            }
           
        }

        public void numericAndLowerAndAlpha(MainForm form, string prefix, int passL, SHA1CryptoServiceProvider shaProv, int chainL, int chainNo)
        {
            Console.WriteLine("Soft generating");
            //Console.WriteLine("\npassL: " + passL);
            //Console.WriteLine("chainNo: " + chainNo);
            //Console.WriteLine("chainL: " + chainL);

            if (prefix.Length == passL)
                chainsCounter++;
            
            //generate numeric, lower-alpha and alpha (62 chars)
            for (int c = firstNumeric; c <= lastLower; c++)
            {
                if ((c > 57 && c < 65) || (c > 90 && c < 97))
                {
                    continue;
                }
                else
                {
                    if (prefix.Length == passL)
                    {
                        lock (this.reductionList)
                        {
                            if (reductionList.ContainsKey(prefix))
                                continue;
                            else
                            {
                                //
                                generateRainbowRow(prefix, passL, shaProv, chainL);
                            }
                        }
                        
                    }
                    if (prefix.Length == passL)
                        return;

                    if (chainsCounter < chainNo)
                        numericAndLowerAndAlpha(form, prefix + (char)c, passL, shaProv, chainL, chainNo);
                    if (chainsCounter == chainNo)
                    {
                        form.tableGenerated();
                        chainsCounter +=1;
                    }
                        

                }
            }

        }

        public void numeric(string prefix, int passL, SHA1CryptoServiceProvider shaProv, int chainL, int chainNo)
        {
            if (prefix.Length == passL)
                chainsCounter++;

            //generate numeric
            for (int c = firstNumeric; c <= lastNumeric; c++)
            {
                if (prefix.Length == passL)
                {
                    generateRainbowRow(prefix, passL, shaProv, chainL);
                    Console.WriteLine("Counter: " + chainsCounter + ", pass: " + prefix);
                }
                
                if (prefix.Length == passL)
                    //Kod redukcji dla prefix'u jako plaintextu o dlugosci stringsLength 
                    //(jesli tutaj bedzie kod redukcji to redukcja bedzie wykonywana tylko dla 
                    //hasel o dlugosci stringslength bez hasel krotszych
                    //string reduction
                    return;
                if (chainsCounter < chainNo)
                {
                    
                    numeric(prefix + (char)c, passL, shaProv, chainL, chainNo);
                }

            }
        }
       

    }
}
