﻿using RainbowServer.DB;
using RainbowServer.Ethernet;
using RainbowServer.Generator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RainbowServer
{
    public partial class RainbowGeneratorForm : Form
    {
        public string alg { get; set; }
        public List<string> charsets = new List<string>();
        public string reduction { get; set; }
        public int maxL { get; set; }
        public int minL { get; set; }
        public int chainNo { get; set; }
        public int chainL { get; set; }

        int sendPasses = 0;
        Collection<Thread> threads = new Collection<Thread>();
        private PasswordGeneratorFPGA passFPGA = new PasswordGeneratorFPGA();
        List<string> genPasses;

        private EthernetWrapper ethernet;

        public RainbowGeneratorForm()
        {
            InitializeComponent();
        }


        public List<String> getCharset()
        {
            return charsets;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            charsets.Clear();
            bool isNumeric = false;
            bool isAlpha = false;
            bool isLower = false;
            
            //Get alphabet
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        string temp = (string)checkedListBox1.Items[i];
                        if (temp.StartsWith("num"))
                        {
                            temp = "numeric";
                        }
                        else if (temp.StartsWith("low"))
                        {
                            temp = "lower";
                        }
                        else if (temp.StartsWith("alp"))
                        {
                            temp = "alpha";
                        }
                        Console.WriteLine("add: " + temp); 
                        charsets.Add(temp);
                    }
            }

            Console.WriteLine("reduction: " + reduction);
            //Check which reduction method was selected

            //Get min length of pass
            //minL = Convert.ToInt32(numericUpDown1.Value);
            //Get max length of pass
            maxL = Convert.ToInt32(numericUpDown2.Value);
            //Get number of chain
            chainNo = Convert.ToInt32(numericUpDown3.Value);
            //Get length of chain
            chainL = Convert.ToInt32(numericUpDown4.Value);
            if(charsets.Count != 0 && reduction.StartsWith("meth"))
            {
                foreach (string x in charsets)
                {
                    if (x.Equals("numeric"))
                        isNumeric = true;
                    if (x.Equals("alpha"))
                        isAlpha = true;
                    if (x.Equals("lower"))
                        isLower = true;

                    Console.WriteLine("* " + x);
                }
                Console.WriteLine("Metoda redukcji: " + reduction);
                Console.WriteLine("Min L hasla: " + minL);
                Console.WriteLine("Max L hasla: " + maxL);
                Console.WriteLine("Liczba lancuchow: " + chainNo);
                Console.WriteLine("Dlugosc lancuchow: " + chainL);


                //Generate passowrds
                if (isNumeric && isAlpha && isLower)
                {
                    var newThread = new Thread(() => 
                    {
                        passFPGA.numericAndLowerAndAlpha("", maxL, chainL, chainNo, reduction); //generacja listy w PassGenFPGA.cs
                        genPasses = passFPGA.getGeneratedPasses();
                        sendPasses++;
                        ethernet.sendString(genPasses[0], ((message) => {
                            Console.WriteLine("Wysłano: " + message);
                        }));

                    });
                    threads.Add(newThread);
                    newThread.Start();
                }

            }
            else
            {
                MessageBox.Show("Enter all fields in form!");
            }

        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if ( ethernet == null )
            {
                try
                {
                    ethernet = new EthernetWrapper(ipBox.Text, int.Parse(portBox.Text));
                    ethernet.runTCPListener(handleAnswer);
                    ((Button)sender).Text = "Disconnect";
                }
                catch
                {
                    Console.Out.WriteLine("### Niepoprawne dane jednostki obliczeniowej");
                }
            }
            else
            {
                ethernet.closeListener();
                ethernet.client.Close();
                ((Button)sender).Text = "Connect";
            }
        }

        private void handleAnswer(String message)
        {
            try
            {
                RainbowHash hashObj = JSONDeserializer.deserializeMessage(message);
                try
                {
                    hashObj.Pass = JSONDeserializer.GetStringFromAsciiHex(hashObj.Pass);
                    hashObj.Hash = JSONDeserializer.GetStringFromAsciiHex(hashObj.Hash);
                    
                }
                catch
                {
                    Console.Out.WriteLine("### Cannot convert from ASCII");
                }
                RainbowDBManager.writeToDB(hashObj.Pass, hashObj.Hash);
                Console.WriteLine("### rainbowRow wrote to DB: (" + hashObj.Pass + " - " + hashObj.Hash + ")");
                /* Przebieg testu:
                 * Wysłano: 0000
                 * rainbowRow write to DB: (00000 - 02ba1) - czyli git
                 * */

                // send next password if needed
                if (sendPasses < genPasses.Count)
                {
                    ethernet.sendString(genPasses[sendPasses], ((message1) =>
                    {
                        Console.WriteLine("### Wysłano: " + message1);
                    }));
                    sendPasses++;
                    this.Invoke((MethodInvoker)delegate {
                        progressBar1.Value = sendPasses * 100 / genPasses.Count;
                    });
                }
            }
            catch
            {
                Console.Out.WriteLine("### Diagnostic message from unit");
                Console.Out.WriteLine(message);
            }
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            alg = radioButton1.Text;
        }

        private void radioButton2_Click(object sender, EventArgs e)
        {
            alg = radioButton2.Text;
        }

        private void radioButton3_Click(object sender, EventArgs e)
        {
            alg = radioButton3.Text;
        }

        private void radioButton4_Click(object sender, EventArgs e)
        {
            reduction = radioButton4.Text;
        }

        private void radioButton5_Click(object sender, EventArgs e)
        {
            reduction = radioButton5.Text;
        }

        private void radioButton6_Click(object sender, EventArgs e)
        {
            reduction = radioButton6.Text;
        }
    }
}
