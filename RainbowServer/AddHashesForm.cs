﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RainbowServer
{
    public partial class AddHashesForm : Form
    {
        public string[] hashes { get; set; }

        public AddHashesForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int Size = richTextBox1.Lines.Length;
            System.Console.WriteLine("Liczba wierszy: {0}", Size);
            hashes = new string[Size];

            for (int i = 0; i <= richTextBox1.Lines.Length - 1; i++)
            {
                hashes[i] = richTextBox1.Lines[i];
                System.Console.WriteLine(hashes[i]);


            }
            richTextBox1.Clear();
            this.Close();
        }
    }
}
