﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RainbowServer.DB
{
    public class RainbowHash
    {
        public int Id { get; set; }
        [JsonProperty("pass")]
        public string Pass { get; set; }
        [JsonProperty("red")]
        public string Hash { get; set; }
    }
}
