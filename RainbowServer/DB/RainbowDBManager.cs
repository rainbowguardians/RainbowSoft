﻿using LiteDB;
using System;
using System.Linq;
using System.Threading;

namespace RainbowServer.DB
{
    class RainbowDBManager
    {
        private static string DatabaseFile = "rainbowTables.db";
        private static string DatabaseCollection = "rainbowHash";

        public static void writeToDB(string password, string hash)
        {
            new Thread(() =>
            {
                using (var db = new LiteDatabase(DatabaseFile))
                {
                    var collection = db.GetCollection<RainbowHash>(DatabaseCollection);

                    db.BeginTrans();

                    collection.Insert(new RainbowHash { Pass = password, Hash = hash });

                    db.Commit();
                }
            }).Start();
        }

        public static void findPassForHash(string hash, Action<string, string> successHandler, Action<string> failureHandler)
        {
            new Thread(() =>
            {
                using (var db = new LiteDatabase(DatabaseFile))
                {
                    var collection = db.GetCollection<RainbowHash>(DatabaseCollection);

                    var results = collection.Find(x => x.Hash == hash);

                    if (results.Count() > 0)
                    {
                        successHandler(results.First().Pass, hash);
                    }
                    else
                    {
                        failureHandler(hash);
                    }
                }
            }).Start();
        }

        public static string getLastPass()
        {
            using (var db = new LiteDatabase(DatabaseFile))
            {
                var collection = db.GetCollection<RainbowHash>(DatabaseCollection);
                var query = collection.FindAll();
                if (query.Count() > 0)
                {
                    return query.Last().Pass;
                }
                return "";
            }
        }
    }
}
