﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RainbowServer.Ethernet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RainbowServer.Ethernet.Tests
{
    [TestClass()]
    public class JSONDeserializerTests
    {
        [TestMethod()]
        public void deserializeMessageTest()
        {
            var testString = "{\"pass\":\"test1\", \"red\":\"testRed\"}";
            var testObject = JSONDeserializer.deserializeMessage(testString);

            Assert.IsTrue(testObject.Pass.Equals("test1") && testObject.Hash.Equals("testRed"));
        }
    }
}